import json
import os
import re
import colorsys
import numpy as np
import pandas as pd
import seaborn as sns
from sklearn.preprocessing import MultiLabelBinarizer
from sklearn.metrics import f1_score
from scipy.sparse import csr_matrix
import matplotlib.pylab as plt

# Common
SE_SPECIALITY_ID = 'ef2aa0915a3bf179755b8915'  # Ids are corrupted
ML_SPECIALITY_ID = 'ff2aa0915a3bf179755b8915'  # Ids are corrupted
SKILL_TAG_BLACK_LIST = [
    "data_science__algorithms",
    "design__prototyping",
    "sales__engineering",
    "data_science__programming",
    "design__brand_graphic_design",
    "product__user_experience",
    "product__sales_management",
    "design__web_visual_design",
    "design__data_visualisation",
    "finance__project_management",
    "design__brand_graphic_design",
    "design__brand_design",
    "sales__negotiation",
    "design__product_management",
    "product__growth_hacking",
    "design__sketching",
    "design__project_management",
    "marketing__project_management",
    "finance__investments_management",
    "sales__project_management",
    "finance__other",
    "software_engineering_tool__hadoop",
    "sales__customer_service",
    "finance__other",
    "sales__cold_contacting",
    "finance__banking",
    "finance__financial_institution",
    "finance__investment_fund",
    "finance__private_equity",
    "finance__consultancy",
    "finance__corporate",
    "finance__manufacturing",
    "finance__startup_scaleup",
    "finance__sme",
    "finance__other",
    'software_engineering__software_product_management',
    'software_engineering__software_project_management',
    'software_engineering__management',
]
SKILL_TAG_BLACK_LIST_PATTEN = '(language|company_industry|company_size|company_revenue|.*team_leading)'
SE_SKILLS_PATTERN = '(software_engineering|programming_language)'
SE_TRACKS_PATTERN = '^(software_engineering)__'

SKILL_TAG_IS_FE_PATTERN = '.*(frontend_development|bash|dart|html_css|javascript|php|powershell|aurelia|typescript|angular|backbone|wordpress|ember|firebase|graphql|vuejs|windows|joomla|linux|vaadin|react|nativescript|rest_api|react_native|jquery|construct|caffe|knockoutjs)$'
SKILL_TAG_IS_BE_PATTERN = '.*(backend_development|abap|bash|c_cpp|clojure|csharp|dart|dlang|elixir|unity|erlang|fsharp|golang|groovy|haskell|java|javascript|julia|kotlin|lisp|lua|objective_c|openedge_abl|perl|php|powershell|python|rlang|ruby|code_igniter|web2py|rust|scala|sql|typescript|vb_net|ansible|apache_hadoop|apache_solr|apache_spark|appcelerator|asp_net|aws|azure|wordpress|docker|django|express|firebase|zend|google_apis|google_cloud|firebase|graphql|beego|cassandra|mongodb|laravel|qt|oracle|windows|vagrant|redis|jenkins|linux|elasticsearch|caffe|sinatra|symfony|tensorflow|neo4j|nodejs|spring|sap|riak|mysql|pyramid|nativescript|postgresql|play|rest_api|react_native|rails|nosql|couchbase|chef|cubicweb|martini|drupal|grails|hibernate|open_stack|hive|jsf|kubernetes)'


# Params
MIN_NUMBER_OF_OPENINGS_WITH_SKILL = 5
MIN_NUMBER_OF_SKILLS_IN_OPENING = 3
MAX_NUMBER_OF_SKILLS_IN_OPENING = 15
MIN_NUMBER_OF_OPENINGS_IN_COUNTRY = 100

MIN_NUMBER_OF_PROFILES_WITH_SKILL = 5
MIN_NUMBER_OF_SKILLS_IN_PROFILE = 3
MAX_NUMBER_OF_SKILLS_IN_PROFILE = 15
MIN_NUMBER_OF_USERS_IN_COUNTRY = 2100

# Paths
INPUT_DATA_DIR = 'data_raw/'
INPUT_NEW_DATA_DIR = 'data_new/'
PREPROCESSED_DATA_DIR = 'data/'
BACKUP_PATH = 'backups/'
RAW_COUNTRIES_PATH = INPUT_DATA_DIR + 'countries.csv'
RAW_SPECIALITIES_PATH = INPUT_DATA_DIR + 'specialities.csv'
RAW_LINKEDIN_SKILLS_JSON_PATH = INPUT_DATA_DIR + 'linkedin_skills.json'
RAW_SKILLS_PATH = INPUT_DATA_DIR + 'skills.csv'
RAW_USERS_PATH = INPUT_DATA_DIR + 'users.csv'
NEW_USERS_PATH = INPUT_NEW_DATA_DIR + 'users.csv'
RAW_OPENINGS_PATH = INPUT_DATA_DIR + 'openings.csv'
NEW_OPENINGS_PATH = INPUT_NEW_DATA_DIR + 'openings.csv'
PREPROCESSED_COUNTRIES_PATH = PREPROCESSED_DATA_DIR + 'countries.csv'
PREPROCESSED_SE_OPENINGS_PATH = PREPROCESSED_DATA_DIR + 'se_openings.csv'
PREPROCESSED_SE_OPENINGS_SKILLS_PATH = PREPROCESSED_DATA_DIR + 'se_openings.skills.csv'
PREPROCESSED_SE_PROFILES_PATH = PREPROCESSED_DATA_DIR + 'se_profiles.csv'
PREPROCESSED_SE_PROFILES_SKILLS_PATH = PREPROCESSED_DATA_DIR + 'se_profiles.skills.csv'
PREPROCESSED_SE_SKILLS_PATH = PREPROCESSED_DATA_DIR + 'se_skills.csv'
PREPROCESSED_SE_TRACKS_PATH = PREPROCESSED_DATA_DIR + 'se_tracks.csv'
PREPROCESSED_SKILLS_PATH = PREPROCESSED_DATA_DIR + 'skills.csv'
PREPROCESSED_SPECIALITIES_PATH = PREPROCESSED_DATA_DIR + 'specialities.csv'
PREPROCESSED_USERS_PATH = PREPROCESSED_DATA_DIR + 'users.csv'

# Labels
COUNTRY_ID_LABEL = 'ID'
COUNTRY_CODE_LABEL = 'COUNTRY_CODE'
CLUSTER_LABEL = 'CLUSTER'
TITLE_LABEL = 'TITLE'
ID_LABEL = 'ID'
TYPE_LABEL = 'TYPE'
SENIORITY_LABEL = 'SENIORITY'

SPECIALITY_ID_LABEL = 'ID'
SPECIALITY_NAME_LABEL = 'SPECIALITY_NAME'

SKILL_ID_LABEL = 'ID'
SKILL_USER_ID_LABEL = 'USER_ID'
SKILL_OPENING_ID_LABEL = 'OPENING_ID'
SKILL_NAME_LABEL = 'NAME'
SKILL_TAG_LABEL = 'TAG'
IS_TRACK_NAME_LABEL = 'IS_TRACK_NAME'
IS_BE_LABEL = 'IS_BE'
IS_FE_LABEL = 'IS_FE'

USER_ID_LABEL = 'ID'
USER_COUNTRY_ID_LABEL = 'COUNTRY_ID'
USER_COUNTRY_CODE_LABEL = 'COUNTRY_CODE'
USER_LAST_POSITION_TITLE_LABEL = 'LAST_POSITION_TITLE'
USER_SKILLS_LABEL = 'SKILLS'
USER_SENIORITY_LABEL = 'SENIORITY'
USER_SPECIALITY_ID_LABEL = 'SPECIALITY_ID'
USER_MATCHING_PROFILE_ID_LABEL = 'MATCHING_PROFILE_ID'

OPENING_ID_LABEL = 'ID'
OPENING_COUNTRY_ID_LABEL = 'COUNTRY_ID'
OPENING_SENIORITY_LABEL = 'SENIORITY'
OPENING_COUNTRY_CODE_LABEL = 'COUNTRY_CODE'
OPENING_TITLE_LABEL = 'TITLE'
OPENING_SKILLS_LABEL = 'SKILLS'
OPENING_SPECIALITY_ID_LABEL = 'SPECIALITY_ID'


# Counties

def get_country_code_by_id(id, df_countries):
    results = df_countries[df_countries[COUNTRY_ID_LABEL] == id]
    if results.shape[0] == 0:
        print("\x1b[31m\"Country with id = '%s' is not found!\"\x1b[0m" % id)
        return None

    return results.iloc[0][COUNTRY_CODE_LABEL]


# Specialities

def get_speciality_name_by_id(id, df_specialities):
    results = df_specialities[df_specialities[SPECIALITY_ID_LABEL] == id]
    return results.iloc[0][SPECIALITY_NAME_LABEL]


# Skills


def map_se_skill_tags(tag: str):
    if tag == 'software_engineering__kubernetes':
        return 'software_engineering_tool__kubernetes'
    return tag


def is_se_skill_tag_valid(tag: str):
    return bool(re.match(SE_SKILLS_PATTERN, tag)) and is_skill_tag_valid(tag)

def is_se_skill_be(tag: str):
    return bool(re.match(SKILL_TAG_IS_BE_PATTERN, tag)) and is_skill_tag_valid(tag)

def is_se_skill_fe(tag: str):
    return bool(re.match(SKILL_TAG_IS_FE_PATTERN, tag)) and is_skill_tag_valid(tag)


def is_track(tag: str):
    # TODO: check if all specs are available
    return bool(re.match(SE_TRACKS_PATTERN, tag)) and is_skill_tag_valid(tag)


def is_skill_tag_valid(tag: str):
    return not (tag in SKILL_TAG_BLACK_LIST) and not (re.match(SKILL_TAG_BLACK_LIST_PATTEN, tag))


def get_skill_index_by_id(id, df_skills):
    results = df_skills[df_skills[SKILL_ID_LABEL] == id]
    if results.shape[0] == 0:
        return None

    return results.index[0]


def get_skill_id_by_index(index, df_skills): return df_skills.loc[index][SKILL_ID_LABEL]


def get_skill_name_by_index(index, df_skills): return df_skills.loc[index][SKILL_NAME_LABEL]


def get_skill_tag_by_index(index, df_skills): return df_skills.loc[index][SKILL_TAG_LABEL]


# Users

def get_user_id_by_index(index, df_users): return df_users.loc[index][USER_ID_LABEL]


def sparce_matrix_to_df(sparse_matrix, columns=[]):
    df = pd.DataFrame.sparse.from_spmatrix(sparse_matrix)
    if (len(columns)):
        df.columns = columns
    return df.sparse.to_dense()


def get_users_skills_df(df_users, df_skills, log_missing_skills=False):
    data = []
    skill_indexes = []
    user_indexes = []
    not_found_skills = set()

    for row in df_users[[USER_SKILLS_LABEL]].itertuples(index=True):
        user_index = row[0]
        user_skills = row[1]

        for skill_id in json.loads(user_skills):
            skill_index = get_skill_index_by_id(skill_id, df_skills)
            if (not skill_index):
                not_found_skills.add(skill_id)
                continue

            user_indexes.append(user_index)
            skill_indexes.append(skill_index)
            data.append(1)

    rows = np.amax(user_indexes) + 1
    columns = np.amax(skill_indexes) + 1

    if (log_missing_skills and len(not_found_skills)):
        print('not found skills:', not_found_skills)

    sparse_matrix = csr_matrix((data, (user_indexes, skill_indexes)), shape=(rows, columns))
    return sparce_matrix_to_df(sparse_matrix)


def get_opening_skills_df(df_openings, df_skills, log_missing_skills=False):
    data = []
    skill_indexes = []
    opening_indexes = []
    not_found_skills = set()

    for row in df_openings[[OPENING_SKILLS_LABEL]].itertuples(index=True):
        openning_index = row[0]
        skills = row[1]

        for skill in json.loads(skills):
            skill_id = skill['skillId']
            is_mandatory = skill.get('isMandatory', False)

            skill_index = get_skill_index_by_id(skill_id, df_skills)
            if (not skill_index):
                not_found_skills.add(skill_id)
                continue

            opening_indexes.append(openning_index)
            skill_indexes.append(skill_index)
            data.append(1)

    rows = np.amax(opening_indexes) + 1
    columns = np.amax(skill_indexes) + 1

    if (log_missing_skills and len(not_found_skills)):
        print('not found skills:', not_found_skills)

    sparse_matrix = csr_matrix((data, (opening_indexes, skill_indexes)), shape=(rows, columns))
    return sparce_matrix_to_df(sparse_matrix)


# Openings

def get_opening_id_by_index(index, df_openings): return df_openings.loc[index][OPENING_ID_LABEL]


# Profiles

def get_words_from_wordcounts(df_word_counts):
    labels = df_word_counts.columns
    sentenses = []
    for _ix, row in df_word_counts.iterrows():
        sentenses.append(list(labels[row > 0].sort_values(ascending=False)))

    return sentenses


def evaluate_by_restored_count(expected_skills: list, predicted_skills: list):
    if not (len(expected_skills)):
        print('Error: expected_skills length is 0')
        return 0

    restored = [skill for skill in predicted_skills if skill in expected_skills]
    return len(restored) / len(expected_skills)


# List helpers
def list_flatten(a):
    return [item for sublist in a for item in sublist]

def list_filter(a, b):
    return list(filter(lambda item: item not in b, a))


def list_mask(a, b):
    return list(filter(lambda item: item in b, a))


# OS

def play_sound():
    os.system("printf '\a'")


def store_to_csv(array, name: str):
    np.savetxt(BACKUP_PATH + name + '.csv', array, delimiter=',')


def load_from_csv(name: str):
    return np.loadtxt(BACKUP_PATH + name + '.csv', delimiter=',')


def np_store(array, name: str):
    np.save(BACKUP_PATH + name, array)


def np_restore(name: str):
    return np.load(BACKUP_PATH + name + '.npy')


# Metrics

# Source: https://gist.github.com/lohithmunakala/2abe086442c83893102a64bdd22cb625
def f1_sampled(actual, pred):
    #converting the multi-label classification to a binary output
    mlb = MultiLabelBinarizer()
    actual = mlb.fit_transform(actual)
    pred = mlb.fit_transform(pred)

    #fitting the data for calculating the f1 score
    f1 = f1_score(actual, pred, average = "samples")
    return f1


# Plotting

# Source: https://stackoverflow.com/questions/28999287/generate-random-colors-rgb
def get_colors(num_colors):
    colors = []
    for i in np.arange(0., 360., 360. / num_colors):
        hue = i / 360.
        lightness = (40 + np.random.rand() * 10) / 100.
        saturation = (90 + np.random.rand() * 10) / 100.
        colors.append(colorsys.hls_to_rgb(hue, lightness, saturation))
    return colors


def plot_top_skills(df, df_skills, skill_label=SKILL_NAME_LABEL, n=50):
    if not (df.shape[0]):
        print('no skills provided!')
        return

    valid_skill_names = list(df_skills[skill_label])
    top_skills = df[[skill_name for skill_name in df.columns if skill_name in valid_skill_names]].sum()
    if not (top_skills.shape[0]):
        print('no skills provided!')
        return
    top_skills = top_skills[top_skills > 0].nlargest(n, keep='all')
    top_skills.plot(kind='bar', figsize=[20, 10])
    plt.show()


# Source: https://github.com/DTrimarchi10/confusion_matrix/blob/master/cf_matrix.py
def make_confusion_matrix(cf,
                          group_names=None,
                          categories='auto',
                          count=True,
                          percent=True,
                          cbar=True,
                          xyticks=True,
                          xyplotlabels=True,
                          sum_stats=True,
                          figsize=None,
                          cmap='Blues',
                          title=None):
    '''
    This function will make a pretty plot of an sklearn Confusion Matrix cm using a Seaborn heatmap visualization.
    Arguments
    ---------
    cf:            confusion matrix to be passed in
    group_names:   List of strings that represent the labels row by row to be shown in each square.
    categories:    List of strings containing the categories to be displayed on the x,y axis. Default is 'auto'
    count:         If True, show the raw number in the confusion matrix. Default is True.
    normalize:     If True, show the proportions for each category. Default is True.
    cbar:          If True, show the color bar. The cbar values are based off the values in the confusion matrix.
                   Default is True.
    xyticks:       If True, show x and y ticks. Default is True.
    xyplotlabels:  If True, show 'True Label' and 'Predicted Label' on the figure. Default is True.
    sum_stats:     If True, display summary statistics below the figure. Default is True.
    figsize:       Tuple representing the figure size. Default will be the matplotlib rcParams value.
    cmap:          Colormap of the values displayed from matplotlib.pyplot.cm. Default is 'Blues'
                   See http://matplotlib.org/examples/color/colormaps_reference.html
                   
    title:         Title for the heatmap. Default is None.
    '''

    # CODE TO GENERATE TEXT INSIDE EACH SQUARE
    blanks = ['' for i in range(cf.size)]

    if group_names and len(group_names) == cf.size:
        group_labels = ["{}\n".format(value) for value in group_names]
    else:
        group_labels = blanks

    if count:
        group_counts = ["{0:0.0f}\n".format(value) for value in cf.flatten()]
    else:
        group_counts = blanks

    if percent:
        group_percentages = ["{0:.2%}".format(value) for value in cf.flatten() / np.sum(cf)]
    else:
        group_percentages = blanks

    box_labels = [f"{v1}{v2}{v3}".strip() for v1, v2, v3 in zip(group_labels, group_counts, group_percentages)]
    box_labels = np.asarray(box_labels).reshape(cf.shape[0], cf.shape[1])

    # CODE TO GENERATE SUMMARY STATISTICS & TEXT FOR SUMMARY STATS
    if sum_stats:
        # Accuracy is sum of diagonal divided by total observations
        accuracy = np.trace(cf) / float(np.sum(cf))

        # if it is a binary confusion matrix, show some more stats
        if len(cf) == 2:
            # Metrics for Binary Confusion Matrices
            precision = cf[1, 1] / sum(cf[:, 1])
            recall = cf[1, 1] / sum(cf[1, :])
            f1_score = 2 * precision * recall / (precision + recall)
            stats_text = "\n\nAccuracy={:0.3f}\nPrecision={:0.3f}\nRecall={:0.3f}\nF1 Score={:0.3f}".format(
                accuracy, precision, recall, f1_score)
        else:
            stats_text = "\n\nAccuracy={:0.3f}".format(accuracy)
    else:
        stats_text = ""

    # SET FIGURE PARAMETERS ACCORDING TO OTHER ARGUMENTS
    if figsize == None:
        # Get default figure size if not set
        figsize = plt.rcParams.get('figure.figsize')

    if xyticks == False:
        # Do not show categories if xyticks is False
        categories = False

    # MAKE THE HEATMAP VISUALIZATION
    plt.figure(figsize=figsize)
    sns.heatmap(cf, annot=box_labels, fmt="", cmap=cmap, cbar=cbar, xticklabels=categories, yticklabels=categories)

    if xyplotlabels:
        plt.ylabel('True label')
        plt.xlabel('Predicted label' + stats_text)
    else:
        plt.xlabel(stats_text)

    if title:
        plt.title(title)


def plot_embeded(embeded, clusters, white_list=None, index=None, markers=None, legend=False, save_name=None):
    df_embeded = pd.DataFrame({'PC1': np.array(embeded)[0], 'PC2': np.array(embeded)[1], 'cluster': clusters})
    if not (markers is None):
        df_embeded['markers'] = markers

    if not (index is None):
        df_embeded = df_embeded.loc[index].copy()

    if not (white_list is None):
        df_embeded = df_embeded[df_embeded['cluster'].isin(white_list)]

    clusters_n = len(np.unique(df_embeded['cluster']))

    sns.scatterplot(data=df_embeded, x="PC1", y="PC2",
                    hue='cluster',
                    style='markers' if not (markers is None) else None,
                    palette=sns.color_palette("hls", clusters_n),
                    legend=legend if not (legend is None) else False
                    )

    if save_name:
        plt.savefig(BACKUP_PATH + save_name + '.png')

    plt.show()


def plot_cluster_stats(data_for_clustering,
                       real_df,
                       clusters,
                       skill_names,
                       track_names,
                       min_size=20,
                       top_n=5,
                       white_list=[],
                       show_all_cluster_sizes=True):
    print('Total number of clusters: ', len(np.unique(clusters)))

    _data_for_clustering = data_for_clustering.copy()
    _data_for_clustering['cluster'] = clusters

    clusters_value_counts = _data_for_clustering['cluster'].value_counts()
    clusters_order = clusters_value_counts[clusters_value_counts >= min_size].sort_values(ascending=False).index
    clusters_n = len(clusters_order)

    print('Total number of usable clusters: ', clusters_n)

    if show_all_cluster_sizes:
        clusters_value_counts.nlargest(clusters_n + 10).plot(kind='bar', figsize=(30, 10),
                                                             title='Number of clusters:' + str(clusters_n))
        plt.show()

    if len(white_list):
        clusters_order = list_mask(clusters_order, white_list)

    for cluster in clusters_order[0:top_n or clusters_n]:
        fig, (ax1, ax2, ax3, ax4) = plt.subplots(1, 4)
        fig.suptitle('Cluster: ' + str(cluster) + '. size: ' + str(clusters_value_counts[cluster]))

        cluster_data = data_for_clustering[clusters == cluster].copy()
        cluster_real_data = real_df.loc[cluster_data.index].copy()

        for skill in skill_names:
            cluster_data[skill] = np.where(cluster_data[skill] > 0, 1, 0)

        cluster_data[track_names].sum().plot(kind='bar', ax=ax1, title='tracks')
        cluster_data[skill_names].sum().sort_values(ascending=False).nlargest(17).plot(kind='bar', ax=ax2,
                                                                                       title='skills', figsize=(15, 5))
        cluster_real_data[SENIORITY_LABEL].value_counts().plot(kind='bar', ax=ax3, title='seniority')
        cluster_real_data[TYPE_LABEL].value_counts().plot(kind='bar', ax=ax4, title='opening/profile')

        plt.show()
