# Sources:
# https://skeptric.com/normalise-job-title-words/


import re
import numpy as np
import unicodedata

from inflection import singularize
from nltk.tokenize import word_tokenize

# import nltk
# nltk.download('punkt')

URL_RE = re.compile('((http|https)\:\/\/)?[a-zA-Z0-9\.\/\?\:@\-_=#]+\.([a-zA-Z]){2,6}([a-zA-Z0-9\.\&\/\?\:@\-_=#%])*', re.IGNORECASE)
EMAIL_RE = re.compile('\S*@\S*', re.IGNORECASE)
EXP_PUNC_RE = re.compile('([\+\-\\\/()\'":,\?\!\&])', re.IGNORECASE)
HASH_TAG_RE = re.compile('#(\w+)', re.IGNORECASE)
PUNC_RE = re.compile('\/|\\|\||,|-|_|>|<|\u2014|\"|\'|\;|\:|\!|\?|\)|\(|\%|\$|\`|\’', re.IGNORECASE)
ROUND_BRACKETS_RE = re.compile('\([^\)]*\)', re.IGNORECASE)
WHITESPACE_RE = re.compile(r'\s+', re.IGNORECASE)

singularize_stop_words = ['data', 'ios', 'sales', 'devops', 'js', 'vue.js', 'vuejs', 'node.js', 'nodejs', 'reactjs',
                          'react.js', 'ops', 'gis', 'dynamics', 'wordpress', 'redis', 'serverless', 'grails',
                          'robotics',
                          'analytics', 'aws', 'sys', 'windows', 'macos', 'continuous', 'angular.js', 'media', 'rails']

white_list_words = [
    'end',  # front end
    'a',  # q a
    'full',
    'stack',
    'front',
    'back',
    'master',
]

stop_words_domain_eng = [
    'department', 'about', 'talent', 'become', 'between',
    'yes', 'no', 'true', 'false',
    'team', 'area',  # leaders
    'co', 'right', 'range',
    'job', 'superjob', 'super',
    'summer', 'winter', 'autumn',
    'unicorn', 'company', 'startup', 'growing',
    'now', 'currently', 'ready', 'only',
    'autonomou', 'independent', 'experienced', 'experience', 'enthusiastic', 'pragmatic', 'passionate', 'passion',
    'professional', 'talented',
    'good',
    'stupid', 'brave', 'creative', 'advanced', 'certified', 'aspiring', 'contract', 'contracted', 'contractor',
    'graduate', 'graduated', 'undergraduate',
    'king', 'expert', 'artisan', 'vizard', 'wizard', 'pro', 'geek', 'magician',
    'freelancer', 'freelance', 'free', 'lance', 'lancer', 'entrepreuner', 'unemployed', 'employed', 'soldier',
    'needed', 'wanted', 'available', 'hiring',
    'level', 'skill', 'opportunity',
    'join', 'apply', 'looking', 'aboard',
    'remote', 'office', 'relocation', 'selfemployed', 'equity', 'studying', 'study',
    'speaking', 'french', 'german', 'fresh'
]

stop_words_places = [
    'aalto', 'tartu', 'tallinn', 'estonia', 'berlin',
    'köln', 'turku', 'turkuun', 'oulu', 'ouluun', 'nortalille', 'ouluun',
    'miami',
    'amsterdam', 'vilniu', 'vilniau', 'riga', 'latvium', 'tinkoff',
]

stop_words_de = [
    'bite', 'bist', 'du'
]

stop_words_ee = [
    'tere', 'terr', 'tiim', 'tiimiin', 'vyriausiasi', 'töötu', 'savininka',
    'kogemustega', 'kogenud', 'kokenut',
    'asiantuntija',
]

stop_words_eng = [
    'rockstar', 'superstar', 'badass', 'ass', 'bad', 'fuck', 'boss', 'bumer', 'kaarel', 'dick', 'bitch',
    'unknown', 'undefined', 'nan', 'nothing', 'bla', 'title', 'todo', 'none', 'blabla',
    'hey', 'hello', 'hi', 'welcome', 'the', 'an', 'still', 'are', 'is', 'am', 'to', 'there', 'and', 'not', 'or',
    'has', 'have', 'any', 'some', 'something',
    'we', 'who', 'my', 'your', 'our', 'you', 'yourself',
    'would', 'until', 'with', 'but',
    'where', 'nowhere', 'what', 'how', 'when',
    'up', 'down', 'year', 'title',
]

stop_words = [
    *stop_words_domain_eng,
    *stop_words_eng,
    *stop_words_places,
]

FOLKSONOMY_PATTERS = {
    # Skills
    'c\s*#': 'c#',
    'c((\s*)(\+))+': 'c++',
    'angular.?js': 'angular',
    'vue.?js': 'vue.js',
    'vue(?![.a-zA-Z])': 'vue.js',
    'node.?js': 'node.js',
    'node(?![.a-zA-Z])': 'node.js',
    'react(iv)?.?native': 'react.native',
    'react.?js': 'react.js',
    'react(?![.a-zA-Z])': 'react.js ',
    'asp\s*(dot|\.)\s*net': 'asp.net',
    '(dot|\.)\s*net': '.net',
    '\.net core': '.net',
    '\.net engineer': '.net developer',
    'go.?lang': 'golang',
    'go(?![.a-zA-Z])': 'golang ',
    'i\s*\/\s*os': 'ios',
    'q\s&\sa': 'quality assurance',

    'sys.?admin$': 'system administrator',

    # Fields
    'dev.?ops?': 'devops',
    '((half|full).?)+\s?s?t([qaou]{,2})c?k?(?![.a-zA-Z])': 'full-stack',
    '(?!=stack)engineer(?![.a-zA-Z])': 'developer',
    '(fronte(d|n|x|t))(?![.a-zA-Z])': 'front-end',
    'fron(t|d)?[\s\-]*e?(n|m)(d|t)': 'front-end',
    'front\s*developer': 'front-end developer',
    'back\s*e(n|m)d': 'back-end',
    'back\s*developer': 'back-end developer',
    '(?!=-\s?end\s?)dev\.?(?![.a-zA-Z])': 'developer',
    '(sofware|softwarw|softrware)': 'software',
    'software\s*(developer|engineer)': 'developer',
    'software\s*(design)(?![.a-zA-Z])': 'software',
    'softwqre|softwere|softwate|softwarre|softwaer|softvare|sofrware': 'software',
    '(veebiarendaja|(we(v|b|bbi)(page)? developer))': 'web developer',

    # Other
    '((part|full|half).{,3})+time': ' ',
    '((free.?lance(r)?)|freelancing)': ' ',
}

MISTAKE_PATTERNS = {
    '^asuurance$': 'assurance',
    '^mageneto$': 'magento',
    '^testija': 'tester',
    '^(admin|admin.?strator)$': 'administrator',
    '^(engineet|elngineer|enginer|enginier|enginner|eningeer|engineere|engineeer|engineef|engeener|engenier|eng\.?)$': 'engineer',
    '^(leadet|leed)$': 'leader',
    'джава': 'java',
    '^(tarkvaraarendaja|ohjelmistosuunnittelija|ohjelmistokehittäjä|programmeerija|softwareentwickler(in)?|разр(о|а)ботчик|debeloper|deceloper|ohjelmoija|developet|develooper|deeveloper|develooer|develoepr|ninja|develoer|develiper|develloper|devolper|develper|develover|developr|developper|devoleper|eveloper|programuotoja|devoloper|devloper|devepoler|coder|hacker|arendaja|kehittäjä|ohjelmoijium|ohjelmistokehittäjiä|koodari|codewriter|scriptwriter|programm?er)$': 'developer',
    '^(seenior|senoir|senjor|senier|seniori|s(e|ê)nior)$': 'senior',
    '^(studend|studenten|studemt|studenta)$': 'student',
    '^(asistente|asiantuntija|assistent)$': 'assistant',
    '^sr\.?$': 'senior',
    '^jr\.?$': 'junior',
    '^mid\.?$': 'middle',
    '^sw\.?$': 'sortware',
    '^javasc?r?ipt$': 'javascript',
    '^advisor$': 'adviser',
    '^win32$': 'windows',
    '^jawa$': 'java',
    '^(lecturer|coach|trainer|instructor|tutor)$': 'teacher',
    '^täispinu$': 'full-stack',
    '^(fullstacker)$': 'full-stack developer',
    '^(vanemarendaja)$': 'senior developer',
    '^(nooremarendaja)$': 'junior developer',
    '^(tarkvara)$': 'software',
    '^(frontender)$': 'front-end developer',
    '^(backender)$': 'back-end developer',
    '^(webmaster|webentwickler)$': 'web developer',
    '^(konsultiksi|consaltant|konsultant)$': 'consultant',
    '^(fontend)$': 'front-end',
    '^(fronttikoodari|前端工程师)$': 'front-end developer',
    '^(mobiilikehittäjä)$': 'mobile developer',
    '^(Automaattestija)$': 'quality assurance automation',
    '^(analyytik|analüütik)$': 'analytic',
    '^(süsteemiarhitekt|järjestelmäasiantuntija)$': 'system architect',
    '^(trchnical|tehnical|technical|techical)$': 'technical',
    '^(测试工程师)$': 'tester',
    '^(andriod|amdroid)$': 'android',
    '^(analust|analystt)$': 'analist',
    '^(teamleiter)$': 'team-leader',
    '^frontending$': 'front-end',
    '^(archirect|arquitech|arhitekt|arhitect|arkkitehti|archidect|architech|archi|archotect|architecht|architecth|architekt)$': 'architect',
    '^developement$': 'development',
    '^(co)?.?founder$': ' ',
    '^[0-9]+$': ' ',
}

ACRONYMS_MAP = {
    'ai': 'artificial intelligence',
    'app': 'application',
    'ar': 'augmented reality',
    'be': 'back-end',
    'bi': 'business intelligence',
    'ceo': 'chief executive officer',
    'cfo': 'chief financial officer',
    'ci': 'continuous integration',
    'cio': 'chief information officer',
    'cto': 'chief technical officer',
    'cxo': 'chief experience officer',
    'cpo': 'chief product officer',
    'db': 'database',
    'dba': 'database administration',
    'fe': 'front-end',
    'it': 'information technology',
    'ml': 'machine learning',
    'ms': 'microsoft',
    'nlp': 'natural language processing',
    'qa': 'quality assurance',
    'rpa': 'robotic process automation',
    'vp': 'vice president',
    'vr': 'virtual reality',
    'xr': 'extended reality',
    'pm': 'project manager',
}

SENIORITY_PATTERNS_MAP = {
    'trainee': 'trainee(ship)?(?![.a-zA-Z])',
    'intern': '(internship|intern(ee)?)(?![.a-zA-Z])',
    'junior': '(juunior|jounior|beginner|juniour|junier|entry.?|(pre.?)?junior)(?![.a-zA-Z])',
    'middle': '(mid.?level|(pre.?)?middle)(?![.a-zA-Z])',
    'senior': '(senior)(?![.a-zA-Z])',
    'lead': '((team|tech).?)?(lead(er|ing|ership)?)(?![.a-zA-Z])',
    'architector': 'architect(ure)?(?![.a-zA-Z])',
}

SENIORITY_VALUE_MAP = {
    'trainee': 1,
    'intern': 1,
    'junior': 1,
    'middle': 2,
    'senior': 3,
    'lead': 3,
    'architector': 3
}

SKILL_PATTERNS_MAP = {
    'aws': '(aws)(?![.a-zA-Z])',
    'serverless': '(serverless)(?![.a-zA-Z])',
    'typescript': '(typescript)(?![.a-zA-Z])',
    'vue.js': '(vue\.js)(?![.a-zA-Z])',
    'node.js': '(node\.js)(?![.a-zA-Z])',
    'angular': '(angular)(?![.a-zA-Z])',
    'react native': '(react\.native)(?![.a-zA-Z])',
    'react.js': '(react(\.?js)?)(?![.a-zA-Z])',
    'javascript': '(javascript|js)(?![.a-zA-Z])',
    'html': '(html5?)(?![.a-zA-Z])',
    'css': '(css)(?![.a-zA-Z])',
    'java': '(java)(?![.a-zA-Z])',
    'flutter': '(flutter)(?![.a-zA-Z])',
    'c#': '(c\#)(?![.a-zA-Z])',
    'c++': '(c\+{2})(?![.a-zA-Z])',
    'asp.net': '(asp\s?\.net)(?![.a-zA-Z])',
    '.net': '(\.net)(?![.a-zA-Z])',
    'sharepoint': '(sharepoint)(?![.a-zA-Z])',
    'php': '(php)(?![.a-zA-Z])',
    'magento': '(magento)(?![.a-zA-Z])',
    'symfony': '(symfony)(?![.a-zA-Z])',
    'laravel': '(laravel)(?![.a-zA-Z])',
    'windows': '(windows)(?![.a-zA-Z])',
    'linux': '(linux|unix)(?![.a-zA-Z])',
    'wordpress': '(wordpress)(?![.a-zA-Z])',
    'python': '(python)(?![.a-zA-Z])',
    'swift': '(swift)(?![.a-zA-Z])',
    'clojure': '(clojure)(?![.a-zA-Z])',
    'grails': '(grails)(?![.a-zA-Z])',
    'agile': '(agile)(?![.a-zA-Z])',
    'azure': '(azure)(?![.a-zA-Z])',
    'dart': '(dart)(?![.a-zA-Z])',
    'abap': '(abap)(?![.a-zA-Z])',
    'drupal': '(drupal)(?![.a-zA-Z])',
    'sap': '(?<![.a-zA-Z])(sap)(?![.a-zA-Z])',
    'kotlin': '(kotlin)(?![.a-zA-Z])',
    'postgresql': '(postgresql)(?![.a-zA-Z])',
    'mysql': '(mysql)(?![.a-zA-Z])',
    'flask': '(flask)(?![.a-zA-Z])',
    'sql': '(sql)(?![.a-zA-Z])',
    'elixir': '(elixir)(?![.a-zA-Z])',
    'oracle': '(oracle)(?![.a-zA-Z])',
    'redis': '(redis)(?![.a-zA-Z])',
    'spring': '(spring(.?boot))(?![.a-zA-Z])',
    'xamarin': '(xamarin)(?![.a-zA-Z])',
    'api': '(api)(?![.a-zA-Z])',
    'powershell': '(powershell)(?![.a-zA-Z])',
    'unity': '(unity(3d)?)(?![.a-zA-Z])',
    'scala': '(scala)(?![.a-zA-Z])',
    'golang': '(golang)(?![.a-zA-Z])',
    'rust': '(rust)(?![.a-zA-Z])',
    'vba': '(vba)(?![.a-zA-Z])',
    'unreal_engine': '(unreal)(?![.a-zA-Z])',
    'ionic': '(ionic)(?![.a-zA-Z])',
    'dynamics': '(microsoft)?(.?(dynamics|365|f(o|0)|ax|d\d+(f0|o)?))+(?![.a-zA-Z])',
    'rails': '(ruby.?on.?)?rails(?![.a-zA-Z])',
    'ruby': 'ruby(?![.a-zA-Z])',
    'matlab': 'matlab(?![.a-zA-Z])',
    'deep learning': '(deep.?learning)(?![.a-zA-Z])',

    # other
    'data dog': 'data.?dog(?![.a-zA-Z])',
    'delphi': 'delphi(?![.a-zA-Z])',
    'ux': '(?<![.a-zA-Z])(ux)(?![.a-zA-Z])',
    'webrtc': '(webrtc)(?![.a-zA-Z])',
    'mean': '(mean?)(?![.a-zA-Z])',
    'mern': '(mern)(?![.a-zA-Z])',
    'ui': '(?<![.a-zA-Z])(ui)(?![.a-zA-Z])',
    'ci_cd': '(continuous (integration|deploy))(?![.a-zA-Z])',
    'microservice': '(micr(o|i).?service)(?![.a-zA-Z])',
}

FIELDS_PATTENS_MAP = {
    # software_engineering__backend_development
    'back-end': '(back(.{1,3})?end)(?![.a-zA-Z])',
    # software_engineering__frontend_development
    'front-end': '(front(.{1,3})?end)(?![.a-zA-Z])',
    # software_engineering__full_stack_development
    'full-stack': '(full(.{1,3})?stack)(?![.a-zA-Z])',
    # software_engineering__devops_engineering
    'devops': '(devops)(?![.a-zA-Z])',
    # software_engineering__mobile_development
    'mobile': '(mobile)(?![.a-zA-Z])',
    'android': '(android)(?![.a-zA-Z])',
    'ios': '(ios)(?![.a-zA-Z])',
    # software_engineering__database_administration
    'database administration': '((data|database) (administration|development))(?![.a-zA-Z])',
    'database': 'database(?![.a-zA-Z])',
    # software_engineering__game_development
    'game': '(indie|game|gamedev|gaming|gameplay|gamer)(?![.a-zA-Z])',
    # software_engineering__machine_learning
    'ml': 'machine learning(?![.a-zA-Z])',
    # software_engineering__embedded_engineering
    'embedded': '(embedd?ed(.?software)?)(?![.a-zA-Z])',
    # software_engineering__iot_engineering
    'iot': '(iot)(?![.a-zA-Z])',
    # software_engineering__robotic_process_automation
    'robotic process automation': '(robotic process automation|robotics)(?![.a-zA-Z])',
    # software_engineering__qa_automation
    'quality assurance': '(code.?quality|quality\s(assurance|assurer|control|controller))(?![.a-zA-Z])',
    'automation': '(automation|automated)(?![.a-zA-Z])',
    # software_engineering__qa_testing
    # software_engineering__data_engineering
    'data analysis': '(data analysis)(?![.a-zA-Z])',
    'data analytic': '(data analytic)(?![.a-zA-Z])',
    'data engineering': '(data developer)(?![.a-zA-Z])',
    # software_engineering__security_engineering
    'security engineering': '(security)(?![.a-zA-Z])',
    'project management': '(project management)(?![.a-zA-Z])',

    # software_engineering__systems_analysis
    # software_engineering__systems_architecture

    # Other
    'data science': '(data (scientist|science))(?![.a-zA-Z])',
    'macos': '(macos)(?![.a-zA-Z])',
    'cloud': '(cloud)(?![.a-zA-Z])',
    'network': '(network)(?![.a-zA-Z])',
    'business intelligence ': '(business intell?igence)(?![.a-zA-Z])',
    'ai': '(artificial\sintelligence)(?![.a-zA-Z])',
    'ar': 'augmented reality(?![.a-zA-Z])',
    'vr': 'virtual reality(?![.a-zA-Z])',
    'development': 'development(?![.a-zA-Z])',
    'engineering': 'engineering(?![.a-zA-Z])',
    'application': 'application(?![.a-zA-Z])',
    'blockchain': '(blockchain)(?![.a-zA-Z])',
    'software': '(software)(?![.a-zA-Z])',
    'management': '(management)(?![.a-zA-Z])',
    'programming': '(programming)(?![.a-zA-Z])',
    'trading': '(trading)(?![.a-zA-Z])',
    'web': '(web(site)?)(?![.a-zA-Z])',
    'it': '(information technology)(?![.a-zA-Z])',
}

ROLE_PATTERNS_MAP = {
    'associate': 'associate(d)?(?![.a-zA-Z])',  # does not affect skills
    'adviser': 'adviser(?![.a-zA-Z])',
    'analytic': '(analyst|analytic|analysist)(?![.a-zA-Z])',
    'assistant': 'assistant(?![.a-zA-Z])',
    'consultant': 'consultant(?![.a-zA-Z])',
    'coordinator': 'coordinator(?![.a-zA-Z])',
    'designer': 'designer(?![.a-zA-Z])',
    'developer': 'developer(?![.a-zA-Z])',
    'engineer': 'engineer(?![.a-zA-Z])',
    'project manager': 'project.?(administrator|manag(er|ment))(?![.a-zA-Z])',
    'product owner': 'product.?owner(?![.a-zA-Z])',
    'manager': '(man(a|e)(g|j)er)(?![.a-zA-Z])',
    'specialist': 'specialist(?![.a-zA-Z])',
    'researcher': 'researcher(?![.a-zA-Z])',
    'student': '(bachelor|student)(?![.a-zA-Z])',
    'teacher': '(teacher)(?![.a-zA-Z])',
    'scientist': '(scientist)(?![.a-zA-Z])',
    'tester': '(tester|testing|test)(?![.a-zA-Z])',
    'digital marketer': '(digital marketer)(?![.a-zA-Z])',
    'cto': '(head\s*of\s*technology|chief\s*(technical|technology)\s*officer)(?![.a-zA-Z])',
    'ceo': 'chief\s*executive\s*officer(?![.a-zA-Z])',
    'cio': 'chief\s*information\s*officer(?![.a-zA-Z])',
    'cxo': 'chief\s*experience\s*officer(?![.a-zA-Z])',
    'cpo': 'chief\s*product\s*officer(?![.a-zA-Z])',

    'system administrator': 'system administrator(?![.a-zA-Z])',
    'administrator': 'administrator(?![.a-zA-Z])',
    'vp': 'vice president(?![.a-zA-Z])',
    'trader': 'trader(?![.a-zA-Z])',
    'scrum master': 'scrum\s*master(?![.a-zA-Z])',
    'system architect': 'system\s*architect(?![.a-zA-Z])',
    'technical director': 'technical\s*director(?![.a-zA-Z])',
    'customer support': 'customer (support|service)(?![.a-zA-Z])'
}


def get_seniority_value(seniority_labels: list):
    return min([SENIORITY_VALUE_MAP.get(label, 0) for label in seniority_labels], default=0)


def apply_patterns(title, patterns):
    for pattern, target in patterns.items():
        title = re.sub(fr'{pattern}', ' ' + target + ' ', title)
        title = compress_whitespace(title)
    return title


def normalize_chars(title):
    title = re.sub(r'с', 'c', title)
    title = re.sub(r'c', 'c', title)
    return title


def compress_whitespace(text):
    return WHITESPACE_RE.sub(r' ', text)


def expand_punctuation(text):
    return EXP_PUNC_RE.sub(r' \1 ', text)


def clean_punctuation(title):
    # title = PUNC_RE.sub(" ", title)
    title = re.sub(r'[^\w\s]', '', title)
    return title


def clean_round_brackets(title):
    return ROUND_BRACKETS_RE.sub('', title)


def clean_emails(title: str):
    return EMAIL_RE.sub('', title)


def clean_hash_tags(title: str):
    return HASH_TAG_RE.sub('', title)


def clean_urls(title: str):
    return URL_RE.sub('', title)


def clean_by_separators(title: str):
    # developer at meetfrank
    # developer in Taru
    # developer from Japan
    title_words = word_tokenize(title)
    for separator in ['at', 'in', 'from', 'for', 'as']:
        if separator in title_words:
            title_words = title_words[:title_words.index(separator)]

    return ' '.join(title_words)


def singularize_words(title_words: list):
    return [singularize(word) if len(word) > 2 and word not in singularize_stop_words else word for word in title_words]


def clean_stop_words(title_words: list, additional_stop_words=[]):
    return [word for word in title_words
            if word in white_list_words or word not in [*additional_stop_words, *stop_words]]


def rewrite_mistakes_in_words(words: list):
    for pattern, target in MISTAKE_PATTERNS.items():
        words = [target if re.search(fr'{pattern}', word.strip()) else word for word in words]

    return words


def expand_acronyms_in_words(title_words: list):
    return [ACRONYMS_MAP.get(word) if len(word) <= 3 and word in ACRONYMS_MAP else word for word in title_words]


def collect_labels(title: str, patterns_map, replace=True):
    labels = []

    for key, pattern in patterns_map.items():
        if re.search(rf'{pattern}', title):
            labels.append(key)
            if replace:
                title = re.sub(pattern, ' ', title)
                title = compress_whitespace(title)

    return (labels, title)


def rewrite_of(term):
    word = r'[\w\d&]+'
    next_word = fr'(?:\s+(?!for\s|or\s){word})'
    following = fr'{word}{next_word}' + '{0,3}'
    regex = fr'({word})\s+of(?:\s+the)?\s+({following})'
    return re.sub(regex, r'\2 \1', term, flags=re.IGNORECASE)


def normalise_title(title: str,
                    additional_stop_words=[],
                    log_labels=False,
                    seniority_value_only=False,
                    labels_only=False):
    title = unicodedata.normalize('NFKC', title)
    title = title.lower()
    title = title.strip()
    title = normalize_chars(title)

    title = clean_emails(title)
    # TODO: fix node.js and asp.net considered as url
    initial_skills, title = collect_labels(title, SKILL_PATTERNS_MAP, replace=False)
    title = clean_urls(title)
    title = clean_hash_tags(title)

    title = expand_punctuation(title)

    title_words = word_tokenize(title)  # 1
    title_words = singularize_words(title_words)  # 2
    title_words = rewrite_mistakes_in_words(title_words)  # 3
    title_words = clean_stop_words(title_words, additional_stop_words=additional_stop_words)  # 4
    title_words = expand_acronyms_in_words(title_words)  # 5
    title = ' '.join(title_words)

    title = apply_patterns(title, FOLKSONOMY_PATTERS)

    other_skills, title = collect_labels(title, SKILL_PATTERNS_MAP)
    fields, title = collect_labels(title, FIELDS_PATTENS_MAP)
    roles, title = collect_labels(title, ROLE_PATTERNS_MAP)
    seniority, title = collect_labels(title, SENIORITY_PATTERNS_MAP)

    title = clean_round_brackets(title)
    title = clean_by_separators(title)
    title = clean_punctuation(title)
    title = compress_whitespace(title)

    title = compress_whitespace(title)
    title = title.strip()

    if seniority_value_only:
        return get_seniority_value(seniority)


    labels = {
        'seniority': seniority,
        'skills': list({*initial_skills, *other_skills}),
        'roles': roles,
        'fields': fields
    }

    if log_labels:
        print(labels)

    if labels_only:
        return labels

    # return ' '.join(np.sort(title.split()))
    return title
    # return ' '.join(np.sort(labels))


def restore_seniority_from_title(df, seniority_label, title_label, default_value):
    df = df.copy()
    restored_seniorities = df[df[seniority_label] == 0][title_label].apply(
        lambda x: normalise_title(str(x), seniority_value_only=True))

    print('Restored results:')
    print(restored_seniorities.value_counts())

    for index, value in restored_seniorities.to_frame().iterrows():
        df.loc[index, seniority_label] = value.item() or default_value

    return df
